#include "kalman_filter.h"
#include "tools.h"

using Eigen::MatrixXd;
using Eigen::VectorXd;

// Please note that the Eigen library does not initialize 
// VectorXd or MatrixXd objects with zeros upon creation.

KalmanFilter::KalmanFilter() {}

KalmanFilter::~KalmanFilter() {}

// Initialize Kalman filter
void KalmanFilter::Init(VectorXd &x_in, MatrixXd &P_in, MatrixXd &F_in,
                        MatrixXd &H_in, MatrixXd &R_in, MatrixXd &Q_in) 
{
  x_ = x_in; // 4x1
  P_ = P_in; // 4x4
  F_ = F_in; // 4x4
  H_ = H_in; // 2x4 for laser, and 3x4 (Jacobian matrix) for radar 
  R_ = R_in; // 2x2 for laser, and 3x3 for radar
  Q_ = Q_in; // 4x4
}

// Predict new state using the current state and state transition matrix
// Calculate the uncertainty in the new state - state covariance matrix
void KalmanFilter::Predict() 
{
	x_ = F_*x_; // 4x4 * 4x1 = 4x1 
	P_ = F_*P_*(F_.transpose()) + Q_; // 4x4 * 4x4 * 4x4 + 4x4 = 4x4

	return;
}

// Update state and covariance matrix based on the new measurement
void KalmanFilter::Update(const VectorXd &z) 
{
	// Map predicted state x_ to measurement space using measurement model H 
	// Calculate the error y between actual measurement z and predicted measurement H_*x_
	VectorXd y = z - H_*x_; // 2x1 - 2x4 * 4x1 = 2x1

	// System or state uncertainty P is mapped into measurement space 
	// using measurement model H, and measurement noise R is added to that
	// uncertainty
	MatrixXd S = H_*P_*(H_.transpose()) + R_; // 2x4 * 4x4 * 4x2 + 2x2 = 2x2

	// Kalman gain is calculated as product of uncertainties 
	MatrixXd K = P_*(H_.transpose())*(S.inverse()); // 4x4 * 4x2 * 2x2 = 4x2
	MatrixXd I = MatrixXd::Identity(x_.size(), x_.size()); // 4x4

	// Update state vector x_ using Kalman gain K and error y
	x_ = x_ + K*y; // 4x1 + 4x2 * 2x1 = 4x1

	// Update state covariance matrix P_ using Kalman gain K and measurement model H_
	P_ = (I - K*H_)*P_; // (4x4 - 4x2 * 2x4)*4x4 = 4x4

	return;
}

void KalmanFilter::UpdateEKF(const VectorXd &z) 
{
	Tools tools;
	// Map predicted state x_ to measurement space using measurement function  
	// Calculate the error y between actual measurement z and predicted measurement h(x_)
	VectorXd y = z - tools.ConvertCartesianToPolar(x_); //3x1 - 3x1 = 3x1
	while (y(1) > M_PI) { y(1) -= 2.*M_PI; }
	while (y(1) < -M_PI) { y(1) += 2.*M_PI; }

	// H_ here is Jacobian matrix

	// Kalman gain is calculated as product of uncertainties 
	MatrixXd S = H_ * P_*(H_.transpose()) + R_; // 3x4 * 4x4 * 4x3 + 3x3 = 3x3

	// System or state uncertainty P is mapped into measurement space 
	// using measurement model H, and measurement noise R is added to that
	// uncertainty
	MatrixXd K = P_ * (H_.transpose())*(S.inverse()); // 4x4 * 4x3 * 3x3 = 4x3 
	MatrixXd I = MatrixXd::Identity(x_.size(), x_.size()); // 4x4

	// Update state vector x_ using Kalman gain K and error y
	x_ = x_ + K*y; // 4x1 + 4x2 * 3x1 = 4x1

	// Update state covariance matrix P_ using Kalman gain K and measurement model H_
	P_ = (I - K*H_)*P_; // (4x4 - 4x3 * 3x4)*4x4 = 4x4

	return;
}
