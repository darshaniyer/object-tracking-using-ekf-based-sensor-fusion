#include "FusionEKF.h"
#include "tools.h"
#include "Eigen/Dense"
#include <iostream>

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;

/*
 * Constructor.
 */
FusionEKF::FusionEKF() 
{
  is_initialized_ = false;

  use_laser_ = true;

  use_radar_ = true;

  previous_timestamp_ = 0;

  // initializing matrices
  R_laser_ = MatrixXd(2, 2);
  R_radar_ = MatrixXd(3, 3);
  H_laser_ = MatrixXd(2, 4);
  Hj_radar_ = MatrixXd(3, 4);

  //measurement function - laser
  H_laser_ << 1, 0, 0, 0,
	          0, 1, 0, 0;

  //measurement covariance matrix - laser
  R_laser_ << 0.0225, 0,
              0,      0.0225;

  //measurement function - radar
  Hj_radar_ << 1, 0, 0, 0,
			   0, 1, 0, 0,
	           0, 0, 1, 0;

  //measurement covariance matrix - radar
  R_radar_ << 0.09, 0,      0,
              0,    0.0009, 0,
              0,    0,      0.09;
}

/**
* Destructor.
*/
FusionEKF::~FusionEKF() {}

/*
* @param {MeasurementPackage} meas_package The latest measurement data of
* either radar or laser.
* Pipeline steps:
1) Initialize
2) Prediction step (apply process model)
	a) Update state transition matrix F
	b) Update process noise covariance matrix Q
	c) Predict 
3) Update step (sensor dependent)
	a) Update state x
	b) Update state covariance matrix P
*/
void FusionEKF::ProcessMeasurement(const MeasurementPackage &measurement_pack) 
{
  /*****************************************************************************
   *  Initialization
   ****************************************************************************/
  if (!is_initialized_) 
  {
    /**
      * Initialize the state ekf_.x_ with the first measurement.
      * Create the covariance matrix.
      * Remember: you'll need to convert radar from polar to cartesian coordinates.
    */
    // first measurement
    cout << "EKF: " << endl;
    ekf_.x_ = VectorXd(4);
    ekf_.x_ << 1, 1, 1, 1;

	ekf_.P_ = MatrixXd(ekf_.x_.size(), ekf_.x_.size()); // 4x4
		
	ekf_.P_ << 0.025, 0, 0, 0,
			   0, 0.025, 0, 0,
			   0, 0, 0.025, 0,
			   0, 0, 0, 0.025;			   

	ekf_.F_ = MatrixXd(ekf_.x_.size(), ekf_.x_.size()); // 4x4
	ekf_.F_ << 1, 0, 1, 0,
			   0, 1, 0, 1,
			   0, 0, 1, 0,
		       0, 0, 0, 1;

	ekf_.Q_ = MatrixXd(ekf_.x_.size(), ekf_.x_.size()); // 4x4
	ekf_.Q_ << 0, 0, 0, 0,
		       0, 0, 0, 0,
			   0, 0, 0, 0,
		       0, 0, 0, 0;

	ekf_.Init(ekf_.x_, ekf_.P_, ekf_.F_, H_laser_, R_laser_, ekf_.Q_);

    if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) 
	{
		/**
		Convert radar from polar to cartesian coordinates and initialize state.
		*/
		VectorXd z = measurement_pack.raw_measurements_;
		double px = z[0] * cos(z[1]);
		double py = z[0] * sin(z[1]);
		ekf_.x_ << px, py, 0.0, 0.0;		
    }
    else if (measurement_pack.sensor_type_ == MeasurementPackage::LASER) 
	{
      /**
      Initialize state.
      */
		VectorXd z = measurement_pack.raw_measurements_;
		ekf_.x_ << z[0], z[1], 0.0, 0.0;		
    }

	previous_timestamp_ = measurement_pack.timestamp_;

    // done initializing, no need to predict or update
    is_initialized_ = true;
    return;
  }

  /*****************************************************************************
   *  Prediction
   ****************************************************************************/

  /**
     * Update the state transition matrix F according to the new elapsed time.
      - Time is measured in seconds.
     * Update the process noise covariance matrix.
     * Use noise_ax = 9 and noise_ay = 9 for your Q matrix.
   */
  double dt = (measurement_pack.timestamp_ - previous_timestamp_)/1000000.0;
  previous_timestamp_ = measurement_pack.timestamp_;

  // update the state transition matrix
  ekf_.F_ << 1, 0, dt, 0,
			 0, 1, 0, dt,
			 0, 0, 1, 0,
			 0, 0, 0, 1;

  // update the process noise covariance matrix
  double noise_ax = 9; //sigma_ax squared
  double noise_ay = 9; //sigma_ay squared

  double c1 = dt * dt;
  double c2 = c1 * dt;
  double c3 = c2 * dt;
  ekf_.Q_ << c3 * noise_ax / 4, 0, c2*noise_ax / 2, 0,
		     0,	c3*noise_ay / 4, 0, c2*noise_ay / 2,
			 c2*noise_ax / 2, 0, c1*noise_ax, 0,
			 0, c2*noise_ay / 2, 0, c1*noise_ay;

  ekf_.Predict();

  /*****************************************************************************
   *  Update
   ****************************************************************************/

  /**
     * Use the sensor type to perform the update step.
     * Update the state and covariance matrices.
   */

  if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) 
  {
	  if (use_radar_)
	  {
		  // Radar updates
		  Hj_radar_ = tools.CalculateJacobian(ekf_.x_);
		  ekf_.H_ = Hj_radar_;
		  ekf_.R_ = R_radar_;
		  ekf_.UpdateEKF(measurement_pack.raw_measurements_);
	  }
  } 
  else if (measurement_pack.sensor_type_ == MeasurementPackage::LASER)
  {
	  if (use_laser_)
	  {
		  // Laser updates
		  ekf_.H_ = H_laser_;
		  ekf_.R_ = R_laser_;
		  ekf_.Update(measurement_pack.raw_measurements_);
	  }
  }

  // print the output
  cout << "x_ = " << ekf_.x_ << endl;
  cout << "P_ = " << ekf_.P_ << endl;
}
