#include <iostream>
#include "tools.h"

using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::vector;

Tools::Tools() {}

Tools::~Tools() {}

VectorXd Tools::CalculateRMSE(const vector<VectorXd>& estimations,
                              const vector<VectorXd>& ground_truth) 
{
	VectorXd rmse(4);
	rmse << 1, 2, 3, 4;

	if ( (estimations.size() > 0) && (estimations.size() == ground_truth.size()) )
	{
		rmse << 0, 0, 0, 0;
		for (int i = 0; i < ground_truth.size(); ++i)
		{
			VectorXd residual = ground_truth[i] - estimations[i]; // 4x1 - 4x1 = 4x1
			residual = residual.array()*residual.array();

			rmse += residual; // 4x1 + 4x1 = 4x1
		}

		rmse /= ground_truth.size();

		rmse = rmse.array().sqrt();		
	}
	else
	{
		cout << "Invalid estimation or ground truth data" << endl;
	}

	return rmse;
}

MatrixXd Tools::CalculateJacobian(const VectorXd& x_state) 
{
	MatrixXd Hj(3,4);
	Hj << 0, 0, 0, 0,
		  0, 0, 0, 0,
		  0, 0, 0, 0;

	double px = x_state[0];
	double py = x_state[1];
	double vx = x_state[2];
	double vy = x_state[3];

	double c1 = sqrt(px*px + py*py);
	double c2 = c1*c1;
	double c3 = c1*c2;

	//check division by zero
	if (fabs(c1) < 0.0001) 
	{
		cout << "CalculateJacobian () - Error - Division by Zero" << endl;
		return Hj;
	}

	Hj << px / c1, py / c1, 0, 0,
		  -py / c2, px / c2, 0, 0,
		  py*(vx*py - vy*px) / c3, px*(vy*px - vx*py) / c3, px / c1, py / c1;  

	return Hj;
}

VectorXd Tools::ConvertCartesianToPolar(const VectorXd& x_state)
{
	VectorXd h(3);
	double px = x_state[0];
	double py = x_state[1];
	double vx = x_state[2];
	double vy = x_state[3];

	double rho = sqrt(px*px + py*py);
	double phi = atan2(py, px);

	// Normalize angle
	while (phi < -M_PI) { phi += 2 * M_PI; }
	while (phi > M_PI) { phi -= 2 * M_PI; }

	double range = (px*vx + py*vy)/ rho;

	h << rho, phi, range;

	return h;
}
