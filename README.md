## Bicycle tracking using the sensor fusion algorithm based on extended kalman filter

A self-driving car expert takes input from all the data sources and turns them into coherent picture. For example, a lidar is able to see distances which cameras have hard time with; a radar can see through fog where visible light cannot penetrate. Sensor fusion helps a car or any sensing robot to understand and track its environment which could consist of other cars, barriers on the shoulder of the road, motorcycles, bicycles, or pedestrians walking across the road. 

The goal of this project is to implement sensor fusion algorithm using extended kalman filter (EKF) to combine data from lidar and radar installed on the car to track a bicycle in front of the car.

## Overall steps

The EKF algorithm consists of the following steps as depicted in Figure 1:

* *first measurement* - the filter will receive initial measurements of the bicycle's position relative to the car. These measurements will come from a radar or lidar sensor.
* *initialize state and covariance matrices* - the filter will initialize the bicycle's position based on the first measurement.
* *next measurement* - then the car will receive another sensor measurement after a time period *Δt*.
* *predict* - the algorithm will predict where the bicycle will be after time *Δt*. One basic way to predict the bicycle location after *Δt* is to assume the bicycle's velocity is constant; thus the bicycle will have moved velocity times *Δt*. For   the   EKF project, the velocity is assumed to be constant.
* *update* - the filter compares the "predicted" location with what the sensor measurement says. The predicted location and the measured location are combined to give an updated location. The Kalman filter will put more weight on either the predicted location or the measured location depending on the uncertainty of each value.
* then the car will receive another sensor measurement after a time period *Δt*. The algorithm then does another predict and update step.

[image1]: ./figures/FusionFlow.jpg "Sensor fusion of lidar and radar data using EKF"
![alt text][image1]
**Figure 1 Sensor fusion of lidar and radar data using EKF**

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Extended-Kalman-Filter-Project).

## Code base

The project was implemented in Visual Studio in Windows 10.

1. In the CarND-Extended-Kalman-Filter-Project, open the ekf.sln file.
2. Rebuild Solution.
3. Open the simulator.
4. Click F5 in Visual Studio to debug. This attaches the code to the simulator.
5. Select "Dataset 1" and click "Start" on the simulator.

To compile the code in Linux or Mac OS or Docker environment, 
1. Go to CarND-Extended-Kalman-Filter-Project\src folder
1. Delete main.cpp 
2. Rename main_other_env.cpp as main.cpp.

## Detailed writeup

Detailed report can be found in [_EKF_writeup.md_](EKF_writeup.md).

## Solution video

![](./videos/EKF_demo.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio.

