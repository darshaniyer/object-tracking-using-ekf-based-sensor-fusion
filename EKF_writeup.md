
## Bicycle tracking using the sensor fusion algorithm based on extended Kalman filter

A self-driving car expert takes input from all the data sources and turns them into coherent picture. For example, a lidar is able to see distances which cameras have hard time with; a radar can see through fog where visible light cannot penetrate. Sensor fusion helps a car or any sensing robot to understand and track its environment which could consist of other cars, barriers on the shoulder of the road, motorcycles, bicycles, or pedestrians walking across the road. 

The goal of this project is to implement sensor fusion algorithm using extended Kalman filter (EKF) to combine data from lidar and radar installed on the car to track a bicycle in front of the car.

## Overall steps 

The EKF algorithm consists of the following steps as depicted in Figure 1:

* *first measurement* - the filter will receive initial measurements of the bicycle's position relative to the car. These     measurements will come from a radar or lidar sensor.
* *initialize state and covariance matrix* - the filter will initialize the state and covariance matrix based on the first measurement.
* *next measurement* - then the car will receive another sensor measurement after a time period *Δt*.
* *predict* - the algorithm will predict where the bicycle will be after time *Δt*. One basic way to predict the bicycle   location after *Δt* is to assume the bicycle's velocity *v* is constant; thus the bicycle will have moved $v.Δt$. For the EKF   project, *v* is assumed to be constant.
* *update* - the filter compares the "predicted" location with what the sensor measurement says. The predicted location and the measured location are combined to give an updated location. The Kalman filter will put more weight on either the predicted location or the measured location depending on the uncertainty of each value.
* then the filter will receive another sensor measurement after a time period *Δt*. The algorithm will do another predict and   update step.

[image1]: ./figures/FusionFlow.jpg "Sensor fusion of lidar and radar data using EKF"
![alt text][image1]
**Figure 1 Sensor fusion of lidar and radar data using EKF**


## Definition of variables 

In the Figure 2 depicted below, following is the definition of variables:

* *X* is the mean state vector. For an extended Kalman filter, the mean state vector contains information about the object's position and velocity that you are tracking. It is called the "mean" state vector because position and velocity are represented by a Gaussian distribution with mean *X*.
* *P* is the state covariance matrix, which contains information about the uncertainty of the object's position and velocity. We can think of it as containing standard deviations.
* *k* represents time steps. So $X_{k}$ refers to the object's position and velocity vector at time *k*.
*  The notation *k+1|k* refers to the prediction step. At time *k+1*, you receive a sensor measurement. Before taking into account the sensor measurement to update your belief about the object's position and velocity, you predict where you think the object will be at time *k+1*. You can predict the position of the object at *k+1* based on its position and velocity at time *k*. Hence $X_{k+1|k}$ means that you have predicted where the object will be at *k+1* but have not yet taken the sensor measurement into account.
* $X_{k+1}$ means that we have now predicted where the object will be at time *k+1* and then used the sensor measurement to   update the object's position and velocity.

[image2]: ./figures/FusionDetails.jpg "Sensor fusion variable definitions"
![alt text][image2]
**Figure 2 Sensor fusion variable definitions**

## EKF algorithm generalization 

The EKF algorithm equations are depicted in Figure 3. During the prediction step, we predict the state *x* and covariance matrix *P*. In the figure, *F* is the state transition matrix that models how state changes from time *k* to time *k+1* during prediction. Matrix *Q* represents the process noise that refers to the uncertainty in the prediction step. We assume the object travels at a constant velocity, but in reality, the object might accelerate or decelerate. The prediction step is the same for both the sensors.

The measurement update equations are different for lidar and radar due to the measurements being in different co-ordinate systems. For lidar, the measurements are in cartesian co-ordinates and standard Kalman filter equations are used. For radar, the measurements are in polar co-ordinates and EKF equations are used. Vector *z* is the measurement vector and matrix *H* is the measurement matrix that is used to project state vector *x* to measurement space. For lidar, the vector *z* contains only the positions $p_{x}$ and $p_{y}$, so *H* eliminates the velocity components from state vector *x* and retains only the position components. For radar, vector *z* contains $\rho$, the range, $\phi$, the bearing angle, and $\rho^{.}$, the range rate in polar co-ordinates.
So, matrix *H* is replaced with function *f()* that converts state vector *x* from cartesian to polar co-ordinates. Matrix *R* models measurement noise that represents the uncertainty in the measurements we receive from the sensor. Generally, the parameters for the random noise measurement matrix will be provided by the sensor manufacturer. The error is mapped into matrix *S*, which is obtained by projecting system uncertainty *P* into the measurement space using the measurement matrix *H* plus the measurement noise matrix *R*. Inverse of matrix *S* is mapped to Kalman gain *K*. For radar, matrix *H* is replaced with Jacobian matrix $H_{j}$ while calculating *S*, *K*, and *P*. Finally, we update our state estimate *x* and uncertainty *P* using *K*, *H*, and error *y*.

[image3]: ./figures/EKF_algo_generalization.jpg "Sensor fusion equations"
![alt text][image3]
**Figure 3 Sensor fusion equations**

## Performance check 

Performance check is done using root mean squared error (RMSE), which is the square root of the average squared difference  between the ground truth state vector $x_{truth}$ and estimated state vector *x*.

## Data

Here is a screenshot of the first data file:

The simulator will be using this data file, and feed main.cpp values from it one line at a time.

[image4]: ./figures/Datafile.jpg "Screenshot of the data file"
![alt text][image4]
**Figure 4 Screenshot of the data file**

Each row represents a sensor measurement where the first column tells you if the measurement comes from radar (R) or lidar (L).

For a row containing radar data, the columns are: sensor_type, rho_measured, phi_measured, rhodot_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

For a row containing lidar data, the columns are: sensor_type, x_measured, y_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

Whereas radar has three measurements (rho, phi, rhodot), lidar has two measurements (x, y).


## File structure 

The generalized overall flow is depicted in Figure 5, and consists of initializing Kalman filter variables, predicting where the bicycle is going to be after a time step *Δt*, and updating where our object is based on sensor measurements. Then the prediction and update steps repeat themselves in a loop. To measure how well our Kalman filter performs, we will then calculate root mean squared error comparing the Kalman filter results with the provided ground truth. These three steps (initialize, predict, update) plus calculating RMSE encapsulate the entire EKF project.

[image5]: ./figures/GeneralFlow.jpg "Generalized flow of the EKF algorithm"
![alt text][image5]
**Figure 5 Generalized flow of the EKF algorithm**

Following are the project files in the *src* folder:

* *main.cpp* - 
    - communicates with the Term 2 Simulator receiving data measurements 
    - calls a function to run the Kalman filter in *kalman_filter.cpp*  
    - reads in the data and sends a sensor measurement to *FusionEKF.cpp*
    - calls a function to calculate RMSE in *tools.cpp*
* *FusionEKF.cpp* - has a variable called *ekf_*, which is an instance of a KalmanFilter class.
    - initializes the filter in *ekf_*
    - calls the predict function in *ekf_*
    - calls the update function in *ekf_* 
* *kalman_filter.cpp* - The  KalmanFilter class is defined in kalman_filter.cpp and kalman_filter.h, and contains
    - the predict function
    - update function for lidar
    - update function for radar. 
* *tools.cpp* - contains the functions to calculate RMSE, the Jacobian matrix, and cartesian to polar conversion.

The Term 2 simulator is a client, and the C++ program software is a web server. *main.cpp* reads in the sensor data line by line from the client and stores the data into a measurement object that it passes to the Kalman filter for processing. Also a ground truth list and an estimation list are used for tracking RMSE. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.

Within *main()*,  the code creates an instance of *FusionEKF* class, receives the measurement data by calling the *ProcessMeasurement()*, which is responsible for the initialization of the Kalman filter as well as calling the prediction and update steps of the Kalman filter. Finally, the *main()* will output estimated position and RMSE for each state component to the simulator.

## Results 

Experiments were carried out for the following:
* for different values of initialization for state covariance matrix P and position and velocity components of state *x*; for all the experiments, positions $p_{x}$ and $p_{y}$ and velocity $v_{y}$ were initialized to 0. 
* process noise - $noise_{ax}$ and $noise_{ay}$
* using only radar measurements
* using only lidar measurements
* combined radar and lidar measurements. 


| Sensor   | P     | $v_{x}$ | $noise_{ax}$ | $noise_{ay}$ | $rmse_{px}$| $rmse_{py}$| $rmse_{vx}$ | $rmse_{vy}$ |
|:-------: |:----: |:-------:|:------------:|:------------:|:----------:|:----------:|:-----------:|:-----------:|
| Combined | 1000  |5.0      | 9            | 9            | 0.09       | 0.08       | 0.31        | 0.40        |
| Combined | 1     |5.0      | 9            | 9            | 0.09       | 0.08       | 0.30        | 0.39        |
| Combined | 0.025 |5.0      | 9            | 9            | 0.12       | 0.08       | 0.51        | 0.44        |
| Combined | 0.025 |0.0      | 9            | 9            | 0.18       | 0.19       | 0.58        | 0.70        |
| Combined | 0.025 |5.0      | 1            | 1            | 0.08       | 0.09       | 0.35        | 0.44        |
| Lidar    | 0.025 |5.0      | 15           | 15           | 0.15       | 0.11       | 0.53        | 0.53        |
| Radar    | 0.025 |5.0      | 9            | 9            | 0.21       | 0.35       | 0.52        | 0.77        |
| Combined | 0.025 |5.0      | 9            | 9            | 0.09       | 0.08       | 0.30        | 0.39        |

The stipulated RMSE requirements (<= [.11, .11, 0.52, 0.52]) were achieved under the following conditions: 
* $v_{x}$ was initialized to 5.0 for dataset 1 and -5.0 for dataset 2
* process noise values <= 12
* both radar and lidar sensors were used. 

The initial values of P did not seem to have any effect. Radar was much noisier than lidar. In addition, angle normalization was extremely critical.

Figures 6 and 7 depict the tracking performance of the EKF algorithm for dataset 1 and dataset 2.

[image6]: ./figures/Dataset1_EKF.jpg "Dataset 1: Tracking performance of EKF algorithm"
![alt text][image6]
**Figure 6 Dataset 1: Tracking performance of EKF algorithm**

[image7]: ./figures/Dataset2_EKF.jpg "Dataset 2: Tracking performance of EKF algorithm"
![alt text][image7]
**Figure 7 Dataset 2: Tracking performance of EKF algorithm**


```python
# <video controls src="videos/EKF_demo.mp4" />
```

## Shortcomings of the EKF approach

The EKF algorithm cannot handle very high level of nonlinearity. The jacobian matrix calculation is very sensitive to noise.

## Possible improvements

The unscented Kalman filter (UKF) algorithm and particle filters can handle arbitrary nonlinearity, and are less sensitive to noise as they do not entail jacobian matrix calculations.

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

